package com.candidatura.candidatura;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
public class CandidaturaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CandidaturaApplication.class, args);
	}

}
