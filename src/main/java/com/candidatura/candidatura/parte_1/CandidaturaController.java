package com.candidatura.candidatura.parte_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;



@RestController
public class CandidaturaController {

    private final CandidaturaService candidaturaService;

    @Autowired
    public CandidaturaController(CandidaturaService candidaturaService) { this.candidaturaService = candidaturaService; }

/*    1.	Dado uma lista de números, imprimir a lista de trás para a frente.
    GET http://localhost:8080/listaReversa?lista=25,35,40,21
    Resposta
    HTTP 200 OK {21,40,35,25}
*/

    @GetMapping("/listaReversa")
    public ResponseEntity<int[]> listaReversa(@RequestParam("lista") int listaOriginal[]){
        return new ResponseEntity<>(candidaturaService.listaReversa(listaOriginal), HttpStatus.OK) ;
    }

    @GetMapping("/imprimirImpares")
    public ResponseEntity<int[]> imprimirImpares(@RequestParam("lista") int listaOriginal[]){
        return new ResponseEntity(candidaturaService.imprimirImpares(listaOriginal), HttpStatus.OK) ;
    }

    @GetMapping("/imprimirPares")
    public ResponseEntity<int[]> imprimirPares(@RequestParam("lista") int listaOriginal[]){
        return new ResponseEntity(candidaturaService.imprimirPares(listaOriginal), HttpStatus.OK) ;
    }

    @GetMapping("/tamanho")
    public ResponseEntity tamanho(@RequestParam("palavra") String palavra){
        return new ResponseEntity(candidaturaService.tamanho(palavra), HttpStatus.OK) ;
    }

    @GetMapping("/maiusculas")
    public ResponseEntity maiusculas(@RequestParam("palavra") String palavra){
        return new ResponseEntity(candidaturaService.maiusculas(palavra), HttpStatus.OK) ;
    }

    @GetMapping("/vogais")
    public ResponseEntity vogais(@RequestParam("palavra") String palavra){
        return new ResponseEntity(candidaturaService.vogais(palavra), HttpStatus.OK) ;
    }

    @GetMapping("/consoantes")
    public ResponseEntity consoantes(@RequestParam("palavra") String palavra){
        return new ResponseEntity(candidaturaService.consoantes(palavra), HttpStatus.OK) ;
    }

    @GetMapping("/nomeBibliografico")
    public ResponseEntity nomeBibliografico(@RequestParam("nome") String nome){
        return new ResponseEntity(candidaturaService.nomeBibliografico(nome), HttpStatus.OK) ;
    }


}
