package com.candidatura.candidatura.parte_1;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CandidaturaService {

    public int[] listaReversa(int listaOriginal[]) {
        // int l =0;
        int listaReversa[] = new int[listaOriginal.length];

        for (int i = 0; i < listaOriginal.length; i++) {

            listaReversa[i] = listaOriginal[listaOriginal.length - 1 - i];

        }
        return listaReversa;
    }

    public List imprimirImpares(int listaOriginal[]) {
        // int l =0;

        List impares = new ArrayList();
        for (int i = 0; i < listaOriginal.length; i++) {

            int lista = listaOriginal[i];
            if (lista % 2 != 0) {
                impares.add(lista);

            }
        }
        return impares;
    }

    public List imprimirPares(int listaOriginal[]) {

        List pares = new ArrayList();

        for (int i = 0; i < listaOriginal.length; i++) {

            int lista = listaOriginal[i];
            if (lista % 2 == 0) {
                pares.add(lista);


            }

        }
        return pares;
    }

    public int tamanho(String palavra) {

        Integer tamanhoPalavra = 0;
        tamanhoPalavra = palavra.length();


        return tamanhoPalavra;
    }

    public String maiusculas(String palavra) {

        String maiusculas = palavra.toUpperCase();

        return maiusculas;
    }

    public List vogais(String palavra) {

        List vogal = new ArrayList();

        for (int i = 0; i < palavra.length(); i++) {
            char c = palavra.charAt(i);
            while (c != palavra.length()) {
                if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') {
                    vogal.add(c);
                }
                break;
            }
        }
        return vogal;
    }

    public List consoantes(String palavra) {

        List consoante = new ArrayList();

        for (int i = 0; i < palavra.length(); i++) {
            char c = palavra.charAt(i);
            while (c != palavra.length()) {
                if (c != 'a' && c != 'e' && c != 'i' && c != 'o' && c != 'u') {
                    consoante.add(c);
                }
                break;
            }
        }
        return consoante;
    }

    public String nomeBibliografico(String nome) {


        String[] nomes = nome.split(" ");
        String sobrenome = nomes[nomes.length - 1];
        String nomeEditado = "";
        for (int i = 0; i < nomes.length - 1; i++) {
            nomeEditado = nomeEditado + " " + nomes[i].substring(0,1).toUpperCase() + nomes[i].substring(1);
        }
        return sobrenome.toUpperCase() + "," + nomeEditado;
    }

}
